#ifndef TASK_INFO_H_
#define TASK_INFO_H_

int randNum(int min, int max);

//train functions
void northTrainFcn(void*);
void eastTrainFcn(void*);
void westTrainFcn(void*);

//train communication functions
void trainComFcn(void*);

//oled display functions
void oledDisplayFcn(void*);

//switch control functions
void switchControlFcn(void*);

#endif