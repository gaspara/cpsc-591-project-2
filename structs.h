#ifndef STRUCTS_H_
#define STRUCTS_H_

//simulate boolean type
enum myBool { FALSE = 0, TRUE = 1 };
typedef enum myBool Bool;

extern unsigned char north, east, west;
extern Bool trainPresent, gridlock;
extern unsigned int trainSize, traversalTime;
extern unsigned int counter;
extern unsigned int gridlockDelay;
extern unsigned int traversalTimeDelay;
extern unsigned int northTrainFlash;
extern Bool northTrainFlashOn;
extern unsigned int eastTrainFlash;
extern Bool eastTrainFlashOn;
extern unsigned int westTrainFlash;
extern Bool westTrainFlashOn;

//Task control block
struct taskControlBlock
{
  void (*myTask)(void*);
  void* taskDataPtr;  
};
typedef struct taskControlBlock TCB;

//Data structs
struct TrainDataStruct
{
  unsigned char* direction;
  Bool* trainPresent;
};
typedef struct TrainDataStruct trainData;

struct TrainComStruct
{
  unsigned char* north;
  unsigned char* east;
  unsigned char* west;
  Bool* trainPresent;
  unsigned int* trainSize;
};
typedef struct TrainComStruct trainComStruct;

struct DisplayStruct
{
  unsigned char* north;
  unsigned char* east;
  unsigned char* west;
  Bool* trainPresent;
  unsigned int* trainSize; 
  unsigned int* traversalTime;
};
typedef struct DisplayStruct displayData;

struct SwitchControlStruct
{
  unsigned char* north;
  unsigned char* east;
  unsigned char* west;
  Bool* trainPresent;
  unsigned int* trainSize; 
  unsigned int* traversalTime;
  Bool* gridlock;
};
typedef struct SwitchControlStruct switchData;

#endif