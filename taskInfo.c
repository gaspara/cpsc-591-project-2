#include "structs.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "taskInfo.h"
#include <math.h>
#include <limits.h>
#include "audio.h"
#include "sounds.h"
#include "C:/StellarisWare/boards/ek-lm3s8962/drivers/rit128x96x4.h"
#include "inc/lm3s8962.h"

void trainComFcn(void* dataStruct)
{
  //convert to trainComData type
  trainComStruct* trainComDataConverter = dataStruct;
  
  //dereference parameter
  trainComStruct trainComData = *trainComDataConverter;
  
  //only if there is no train can a new one become available
  if (*trainComData.trainPresent == FALSE) {
    //train is now present
    *trainComData.trainPresent = TRUE;
    
    int direction = randomInteger(0, 2);
    //printf("the number is: %d\n", direction);
    int trainSize = randomInteger(1,9);
    //printf("the", trainSize);
    unsigned char setDirection = '1';
    
    switch (direction)
    {
      case 0:
        *trainComData.north = setDirection;
        break;
      case 1:
        *trainComData.east = setDirection;
        break;
      case 2:
        *trainComData.west = setDirection;
        break;
    }
    
    *trainComData.trainSize = 1;
  }
}


//train communication functions
void northTrainFcn(void* dataStruct)
{
  //convert to trainData type
  trainData* northTrainConverter = dataStruct;
  
  //dereference parameter
  trainData northTrainData = *northTrainConverter;
  
  //AudioOn();
  if (*northTrainData.direction == '1')
  {
    //flash
    if (northTrainFlashOn == FALSE && northTrainFlash == 0) //turn light on
    {
      GPIO_PORTF_DATA_R |= 0x01;
      northTrainFlash = 14;     //10 software delays = 1 sec, 1.5 sec rate
                                //this turn it will be decremented
      northTrainFlashOn = TRUE;
    }
    else if (northTrainFlashOn == TRUE && northTrainFlash == 0) //turn light off
    {
      GPIO_PORTF_DATA_R &= ~(0x01);
      northTrainFlash = 14;     //10 software delays = 1 sec, 1.5 sec rate
                                //this turn it will be decremented
      northTrainFlashOn = FALSE;
    }
    else              //light has not been on/off for the required 1,5 sec
    {
      northTrainFlash--;
    } 
  }
}

void eastTrainFcn(void* dataStruct)
{
  //convert to trainData type
  trainData* eastTrainConverter = dataStruct;
  
  //dereference parameter
  trainData eastTrainData = *eastTrainConverter;
  
  //AudioOn();
  if (*eastTrainData.direction == '1')
  {
    //flash
    if (eastTrainFlashOn == FALSE && eastTrainFlash == 0) //turn light on
    {
      GPIO_PORTF_DATA_R |= 0x01;
      eastTrainFlash = 19;      //10 software delays = 1 sec, 2 sec rate
                                //this turn it will be decremented
      eastTrainFlashOn = TRUE;
    }
    else if (eastTrainFlashOn == TRUE && eastTrainFlash == 0) //turn light off
    {
      GPIO_PORTF_DATA_R &= ~(0x01);
      eastTrainFlash = 19;      //10 software delays = 1 sec, 2 sec rate
                                //this turn it will be decremented
      eastTrainFlashOn = FALSE;
    }
    else              //light has not been on/off for the required 2 sec
    {
      eastTrainFlash--;
    }  
  }
}

void westTrainFcn(void* dataStruct)
{
  //convert to trainData type
  trainData* westTrainConverter = dataStruct;
  
  //dereference parameter
  trainData westTrainData = *westTrainConverter;
  
  //AudioOn();
  if (*westTrainData.direction == '1')
  {
    //flash
    if (westTrainFlashOn == FALSE && westTrainFlash == 0) //turn light on
    {
      GPIO_PORTF_DATA_R |= 0x01;
      westTrainFlash = 9;       //10 software delays = 1 sec, 1 sec rate
                                //this turn it will be decremented
      westTrainFlashOn = TRUE;
    }
    else if (westTrainFlashOn == TRUE && westTrainFlash == 0) //turn light off
    {
      GPIO_PORTF_DATA_R &= ~(0x01);
      westTrainFlash = 9;       //10 software delays = 1 sec, 1 sec rate
                                //this turn it will be decremented
      westTrainFlashOn = FALSE;
    }
    else              //light has not been on/off for the required 1 sec 
    {
      westTrainFlash--;
    }  
  }
}

//oled display functions
void oledDisplayFcn(void* dataStruct)
{
  //convert to displayData type
  displayData* displayConverter = dataStruct;
  
  //dereference parameter
  displayData displayData = *displayConverter;
  
  if (*displayData.trainPresent == TRUE) 
  {
    RIT128x96x4StringDraw("Train present, ", 1, 24, 15);
    if (*displayData.north == '1')
    {
      RIT128x96x4StringDraw("North \n", 88, 24, 15);
    }
    else if(*displayData.east == '1')
    {
      RIT128x96x4StringDraw("East \n", 88, 24, 15);  
    }
    else if(*displayData.west == '1')
    {
      RIT128x96x4StringDraw("West \n", 88, 24, 15);  
    }
    
    RIT128x96x4StringDraw("Size = ", 1, 34, 15);
    char trainSize[2];
    trainSize[0] = *displayData.trainSize + '0';
    trainSize[1] = '\0';
    RIT128x96x4StringDraw(trainSize, 40, 34, 15);
    
    RIT128x96x4StringDraw(", ", 48, 34, 15);
    char traversalTime[3];
    int firstDigit = *displayData.traversalTime / 10;
    if (firstDigit)
    {
      traversalTime[0] = (firstDigit) + '0';
      traversalTime[1] = (*displayData.traversalTime % 10) + '0';
      traversalTime[2] = '\0';
    }
    else
    {
      traversalTime[0] = (*displayData.traversalTime % 10) + '0';
      traversalTime[1] = '\0';
      traversalTime[2] = '\0';
    }
    RIT128x96x4StringDraw(traversalTime, 58, 34, 15);
    RIT128x96x4StringDraw(" sec", 70, 34, 15);
  }
}

//switch control functions
void switchControlFcn(void* dataStruct)
{
  //convert to switchControlData type
  switchData* switchControlConverter = dataStruct;
  
  //dereference parameter
  switchData switchControlData = *switchControlConverter;
  
  //to determine if there is a gridlock
  int gridlockChoice = 0;
  
  //check trainPresent
  if (*switchControlData.trainPresent == TRUE)
  {
    //generate random number only if train not currently traversing intersection
    if (*switchControlData.traversalTime == 0)
    {
      gridlockChoice = randomInteger(-9, 9);
    }
    
    if (*switchControlData.gridlock == TRUE && gridlockDelay > 1 && traversalTimeDelay == 0)
    {
      gridlockDelay--;
    }
    else if (gridlockChoice < 0 && traversalTimeDelay == 0 && gridlockDelay != 1) 
    {
      *switchControlData.gridlock = TRUE;
      //multiply by 60 to turn minutes to seconds
      //multiply by 10 because 10 software delays is 1 second
      gridlockDelay = (int) (0.2 * gridlockChoice * 60 * 10); 
    }
    else           
    {
      //if gridlockDelay = 1, this trun it will be decremented to 0
      //gridlockDelay = 1 or 0, either way gridlock is false
      *switchControlData.gridlock = FALSE;
        
      if (traversalTimeDelay > 1)  //delay; train traversing
      {
        traversalTimeDelay--;
      }
      else if (traversalTimeDelay == 1) //this turn it would be decremented to 0
                                        //train finishes traversing
      {
        //reset variables
        *switchControlData.north = '0';
        *switchControlData.east = '0';
        *switchControlData.west = '0';
        *switchControlData.trainPresent = FALSE;
        *switchControlData.trainSize = 0;
        *switchControlData.traversalTime = 0;
        traversalTimeDelay = 0;
        northTrainFlash = eastTrainFlash = westTrainFlash = 0;
        northTrainFlashOn = eastTrainFlashOn = westTrainFlashOn = FALSE;
        gridlockDelay = 0;
        initializeBoard();            //reset the board and display too
      }
      else {     
        //calculate traversal time
        //multiply by 60 to turn minutes to seconds
        unsigned int traversalTime = (int) 
          (0.1 * (*switchControlData.trainSize) * 60);
        
        *switchControlData.traversalTime = traversalTime;
        
        //calculate traversal time delay
        //multiply by 10 because 10 software delays is 1 second
        traversalTimeDelay = traversalTime * 10;
        
      }
    }
  }
}

//source: http://www.cplusplus.com/reference/cstdlib/srand/
//source: http://www.dbforums.com/delphi-c-etc/1208064-how-generate-random-number-within-0-10-c-language.html
//source: http://stackoverflow.com/questions/822323/how-to-generate-a-random-number-in-c
int randNum(int min, int max) 
{
  int random = rand();
  return (random %(max - min)) + 1; 
}
